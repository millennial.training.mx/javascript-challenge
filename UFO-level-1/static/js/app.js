// Using the UFO dataset provided in the form of an array of JavaScript objects, write code that appends a table to your web page and then adds new rows of data for each UFO sighting.
// Make sure you have a column for datetime, city, state, country, shape, duration and comment.
let tableData = data;

//initialize table body with data
let tablebody = d3.select('tbody');
tableData.forEach(function (ufo_sighting) {
    let trow = tablebody.append('tr');
    Object.entries(ufo_sighting).forEach(function ([key, value]) {
        let tdata = trow.append('td');
        tdata.text(value);
    });
});

// Use a date form in your HTML document and write JavaScript code that will listen for events and search through the date/time column to find rows that match user input.
let filter_btn = d3.select('#filter-btn');
filter_btn.on('click', function () {
    //clear tbody
    tablebody.html('');
    // Filter data by given date
    let filter_date = d3.select('#datetime').property('value');
    let sightings_filtered = tableData.filter(sighting => sighting.datetime === filter_date);
    
    sightings_filtered.forEach(function (selections) {
        console.log(selections);
        let sighting = tablebody.append('tr');
        Object.entries(selections).forEach(function ([key, value]) {
            console.log(key, value);
            let UFOcell = sighting.append('td');
            UFOcell.text(value);
        });
    });
});
